# M.I.A

Welcome to [M]y [I]ntelligent [A]ssistant

---

## Table of contents

- [Features](#Features)
- [Requirements](#Requirements)
- [How to Setup](#How-to-Setup)

## Features

---

- Program arguments
    - `loglevel=debug`/`none`/`errors_only`

      `debug` = shows all logs

      `none` = shows only Logger.log()

      `error_only` = shows warnings and errors
    - `toggletimestamp`

      toggles the timestamp inside every log message
    - `disablelogfile`

      disables the functionality of writing the logs into a file

- Simple Commands

  Provides a simple command class which allows to simply add custom commands

## Requirements

---

> Java 20

## How to Setup

---

> Comming Soon!
