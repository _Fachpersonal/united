package net.nex.mia;

import net.nex.mia.util.logging.Log;
import net.nex.mia.util.logging.LogLevel;
import net.nex.mia.util.logging.LogState;
import net.nex.mia.util.logging.Logger;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class LogTest {

    @Test
    public void DefaultLoggingTest() {
        setupLoggingTests();
        Log logTest;
        logTest = Logger.log("Log Test");
        assertEquals(new Log(LogState.LOG, "Log Test", null, logTest.timeStamp()), logTest);
        logTest = Logger.log("Log Test", "with a detailed Message");
        assertEquals(new Log(LogState.LOG, "Log Test", "with a detailed Message", logTest.timeStamp()), logTest);

        logTest = Logger.warning("Warning Test");
        assertEquals(new Log(LogState.WARNING, "Warning Test", null, logTest.timeStamp()), logTest);
        logTest = Logger.warning("Warning Test", "with a detailed Message");
        assertEquals(new Log(LogState.WARNING, "Warning Test", "with a detailed Message", logTest.timeStamp()), logTest);

        logTest = Logger.error("Error Test");
        assertEquals(new Log(LogState.ERROR, "Error Test", null, logTest.timeStamp()), logTest);
        logTest = Logger.error("Error Test", "with a detailed Message");
        assertEquals(new Log(LogState.ERROR, "Error Test", "with a detailed Message", logTest.timeStamp()), logTest);

        logTest = Logger.debug("Debug Test");
        assertEquals(new Log(LogState.DEBUG, "Debug Test", null, logTest.timeStamp()), logTest);
        logTest = Logger.debug("Debug Test", "with a detailed Message");
        assertEquals(new Log(LogState.DEBUG, "Debug Test", "with a detailed Message", logTest.timeStamp()), logTest);
    }

    private void setupLoggingTests() {
        if (Logger.containsTimeStamp())
            Logger.toggleTimeStamp();
        Logger.setLogLevel(LogLevel.DEBUG);
    }
}
