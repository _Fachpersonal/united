package net.nex.mia;

import net.nex.mia.cli.CLI;
import net.nex.mia.util.DBConnector;
import net.nex.mia.util.logging.LogLevel;
import net.nex.mia.util.logging.Logger;

import java.io.File;
import java.sql.SQLException;

public class App {

    public static String VERSION = "v0.1";
    public static boolean STORE_LOCALLY = false;
    public static String DIRECTORY = null;

    public static void main(String[] args) throws SQLException {
        setup(args);
        if (!STORE_LOCALLY)
            new DBConnector(null, null, null);
        new Thread(new CLI(), "CLI").start();
    }


    private static void setup(String[] args) {
        for (String argument : args) {
            argument = argument.toLowerCase();
            switch (argument) {
                case "loglevel=debug" -> Logger.setLogLevel(LogLevel.DEBUG);
                case "loglevel=log_only" -> Logger.setLogLevel(LogLevel.LOG_ONLY);
                case "loglevel=none" -> Logger.setLogLevel(LogLevel.NONE);
                case "loglevel=errors_only" -> Logger.setLogLevel(LogLevel.ERRORS_ONLY);
                case "toggletimestamp" -> {
                    Logger.toggleTimeStamp();
                    System.out.println("Writing to log is disabled!");
                }
                case "disablelogfile" -> {
                    Logger.disableLogFile();
                    System.out.println("Writing to log is disabled!");
                }
                case "storelocally" -> {
                    STORE_LOCALLY = true;
                    DIRECTORY = System.getProperty("user.home") + "/mia/";
                    File f = new File(DIRECTORY);
                    if (f.mkdir()) {
                        Logger.debug("Local storage initialized [%s]!", DIRECTORY);
                    }
                    Logger.debug("Local Storage activated");
                }
                default -> Logger.warning("Sorry but this argument could not be recognized! [%s]%n", argument);
            }
        }
    }
}
