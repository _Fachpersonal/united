package net.nex.mia.cli;

public class CLIHelper {

    public final static int TAG_PADDING = 10;
    public final static int PROJECT_PADDING = 30;

    public static String getPadding(int textLength, int maxPadding) {
        return " ".repeat(Math.max(0, maxPadding - textLength));
    }

    public static String getTextPreview(String text, int textLength) {
        if (text.length() < textLength) {
            return text;
        }
        return text.substring(0, textLength) + "...";
    }
}
