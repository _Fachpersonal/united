package net.nex.mia.cli;

import net.nex.mia.App;
import net.nex.mia.cli.commands.ProjectManagementCommand;
import net.nex.mia.cli.commands.TestCommand;
import net.nex.mia.util.logging.Logger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class CLI implements Runnable {

    public static ArrayList<CLICommand> commands;
    public static CLI instance;
    private Scanner input;

    public CLI() {
        if (instance == null) {
            instance = this;
        }
        System.out.printf("Welcome to Mia - %s%n", App.VERSION);
        commands = new ArrayList<>();

        registerCommands();
    }

    private void registerCommands() {
        CLICommand[] buffer = new CLICommand[]{
                new TestCommand(),
                new ProjectManagementCommand()
        };

        for (CLICommand cmd : buffer) {
            if (!cmd.command.isBlank() && !cmd.description.isBlank()) {
                commands.add(cmd);
                Logger.debug(String.format("Registered command '%s'", cmd.command));
            }
        }
    }

    @Override
    public void run() {
        input = new Scanner(System.in);
        String line;
        boolean commandFound = false;
        System.out.print(" > ");
        while (!(line = input.nextLine()).contains("exit")) {
            String[] args;
            if (line.contains(" ")) {
                args = getArguments(line);
            } else {
                args = null;
            }
            String command = line.split(" ")[0];
            for (CLICommand cmd : commands) {
                if (!command.equalsIgnoreCase(cmd.command) && !command.equalsIgnoreCase(cmd.alias))
                    continue;

                commandFound = true;
                Logger.debug(String.format("Executing CLICommand '%s' with following arguments '%s'", cmd.command, Arrays.toString(args)));
                cmd.run(args);
            }
            if (!commandFound) {
                Logger.log(String.format("There is no command such as '%s'", command));
                printHelp();
            }
            commandFound = false;
            System.out.print(" > ");
        }
    }

    private void printHelp() {
        System.out.println("--== HELP == --\n");
        for (CLICommand cmd : commands) {
            int numberOfSpaces = 35 - (cmd.command.length() + cmd.alias.length());
            System.out.printf("%s [%s]%" + numberOfSpaces + "s%s", cmd.command, cmd.alias, " ", cmd.description);
            System.out.println();
        }
        System.out.println("\n--== HELP == --");
    }

    public String[] getArguments(String line) {
        ArrayList<String> arguments = new ArrayList<>();
        line = line.substring((line.indexOf(" ") + 1));
        boolean quotation = false;
        StringBuilder quotationArgument = new StringBuilder();
        StringBuilder argument = new StringBuilder();
        for (char c : line.toCharArray()) {
            if (c == '\"') {
                if (quotation) {
                    arguments.add(quotationArgument.toString());
                    quotationArgument = new StringBuilder();
                    quotation = false;
                    continue;
                } else {
                    quotation = true;
                    continue;
                }
            }
            if (c == ' ' && !quotation) {
                arguments.add(argument.toString());
                argument = new StringBuilder();
                continue;
            }
            if (quotation) {
                quotationArgument.append(c);
            } else {
                argument.append(c);
            }
        }
        arguments.add(argument.toString());
        return arguments.toArray(new String[0]);
    }

    public Scanner getInput() {
        return input;
    }
}
