package net.nex.mia.cli;

import net.nex.mia.cli.commands.ProjectManagementCommand;
import net.nex.mia.model.Issue;
import net.nex.mia.model.Project;
import net.nex.mia.util.logging.Logger;

public class CLIOptionsSelector<T> {

    private final String title;
    private final T[] options;

    public CLIOptionsSelector(String title, T[] options) {
        this.title = title;
        this.options = options;
    }

    private void viewOptions() {
        System.out.printf("---=== * %s * ===---%n", title);
        for (int i = 0; i < options.length; i++) {
            if (options[i] instanceof Issue issue) {
                System.out.printf("[%d] (%s-%d) %s[%s] - %s%n", i, ProjectManagementCommand.selectedProject.tag(), issue.index(), issue.title(), issue.state(), issue.description());
            } else if (options[i] instanceof Project project) {
                System.out.printf("[%d] %s(%s) - %s%n", i, project.name(), project.tag(), project.description());
            } else {
                System.out.printf("[%d] %s%n", i, options[i].toString());
            }
        }
        System.out.printf("---=== * %s * ===---%n", title);
    }

    public T selectOption() {
        viewOptions();
        if (options == null || options.length == 0) {
            return null;
        }
        int choice = -1;
        do {
            System.out.println("Please select one of the given options :");
            String strChoice = CLI.instance.getInput().nextLine();
            try {
                choice = Integer.parseInt(strChoice);
            } catch (NumberFormatException ignored) {
            }
            Logger.debug(String.format("Selected option number '%d' with '%d' options", choice, options.length));
        } while (choice < 0 || choice > options.length);
        return options[choice];
    }
}
