package net.nex.mia.cli;

import net.nex.mia.util.logging.Logger;

public abstract class CLICommand {

    public final static short COMMAND_LENGTH = 25;
    protected final String command;
    protected final String description;
    protected final String alias;

    public CLICommand(String alias, String command, String description) {
        if (command.length() >= COMMAND_LENGTH) {
            Logger.error(String.format("Sorry but cannot register command, because the command name is to long (allowed max : %d)", COMMAND_LENGTH));
            this.command = null;
            this.description = null;
            this.alias = null;
            return;
        }
        this.command = command;
        this.description = description;
        this.alias = alias;
    }

    public abstract void run(String[] args);

    public String getCommand() {
        return command;
    }

    public String getDescription() {
        return description;
    }

    public String getAlias() {
        return alias;
    }
}
