package net.nex.mia.cli.commands;

import net.nex.mia.cli.CLI;
import net.nex.mia.cli.CLICommand;

public class TestCommand extends CLICommand {
    public TestCommand() {
        super("t", "test", "Returns a test");
    }

    @Override
    public void run(String[] args) {
        System.out.println("This is just to test some features!");
        System.out.print(" ? Please enter your age : ");
        int age = CLI.instance.getInput().nextInt();
        System.out.printf("Wow, you are already %s years old!%n", age);
    }
}
