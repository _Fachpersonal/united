package net.nex.mia.cli.commands;

import net.nex.mia.cli.CLI;
import net.nex.mia.cli.CLICommand;
import net.nex.mia.cli.CLIOptionsSelector;
import net.nex.mia.model.Issue;
import net.nex.mia.model.Project;
import net.nex.mia.util.logging.Logger;

import java.util.ArrayList;
import java.util.Arrays;

public class ProjectManagementCommand extends CLICommand {

    public static Project selectedProject;
    public static Issue selectedIssue;

    public ProjectManagementCommand() {
        super("pmt", "projectmanagementtool", "With this command you can create/edit/delete Projects and create/edit/delete Issues for each Project");
        selectedProject = null;
        Project.loadAllProjects();
    }

    public static Project selectProject(String[] args) {
        final String projectName;
        if (args != null && args.length >= 1) {
            projectName = args[0];
        } else {
            System.out.println("Please give the name of the project you are trying to select :");
            projectName = CLI.instance.getInput().nextLine();
        }

        ArrayList<Project> projectsWithSameName = new ArrayList<>();
        for (Project oldProject : Project.allProjects) {
            if (oldProject.name().equalsIgnoreCase(projectName) || oldProject.name().toLowerCase().contains(projectName.toLowerCase())) {
                projectsWithSameName.add(oldProject);
            }
        }
        String[] numbers = new String[]{"zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten"};
        Logger.debug(String.format("Found %s " + (projectsWithSameName.size() == 1 ? "project" : "projects"), projectsWithSameName.size() > 10 ? projectsWithSameName.size() : numbers[projectsWithSameName.size()]));

        if (!projectsWithSameName.isEmpty()) {
            CLIOptionsSelector<Project> cliOptionsSelector = new CLIOptionsSelector<>("Projects", projectsWithSameName.toArray(new Project[0]));
            Project project = cliOptionsSelector.selectOption();

            Logger.warning(String.format("The following project was selected! [%s]", project.name()));
            return project;
        } else {
            System.out.printf("Sorry but there are no projects with the given name '%s'%n", projectName);
            return null;
        }
    }

    public static Issue selectIssue(String[] args) {
        if (selectedProject == null) {
            Logger.warning("Please select a project first!");
            return null;
        }
        if (selectedProject.issues().isEmpty()) {
            Logger.warning("You cannot select a Issue because this project does not have any! Create one first");
            return null;
        }
        CLIOptionsSelector<Issue> cliOptionsSelector = new CLIOptionsSelector<>("Issue's", selectedProject.issues().toArray(new Issue[0]));
        Issue oldIssue = cliOptionsSelector.selectOption();
        Logger.warning(String.format("The following issue was selected! [%s]", oldIssue.title()));
        return oldIssue;
    }

    @Override
    public void run(String[] args) {
        if (Project.allProjects.isEmpty()) {
            Project.getProtectedProject();
        }
        if (args == null || args.length == 0) {
            runDefault();
            return;
        }
        String[] newArguments = null;
        if (args.length > 1) {
            newArguments = Arrays.copyOfRange(args, 1, args.length);
        }

        if (args[0].equalsIgnoreCase("createProject")) {
            Project.createProject(newArguments);
//        } else if (args[0].equalsIgnoreCase("editProject")) {
//            Project.editProject(newArguments);
//        } else if (args[0].equalsIgnoreCase("deleteProject")) {
//            Project.deleteProject(newArguments);
        } else if (args[0].equalsIgnoreCase("showAllProjects")) {
            Project.showAllProjects();
        } else if (args[0].equalsIgnoreCase("showAllIssues")) {
            Project.showAllIssues();
        } else if (args[0].equalsIgnoreCase("selectProject")) {
            selectedProject = selectProject(newArguments);
        } else if (args[0].equalsIgnoreCase("selectIssue")) {
            selectedIssue = selectIssue(newArguments);
        } else if (args[0].equalsIgnoreCase("createIssue")) {
            Issue.createIssue(newArguments);
        } else if (args[0].equalsIgnoreCase("showProject")) {
            Project.displayProject(newArguments);
        } else if (args[0].equalsIgnoreCase("showIssue")) {
            Issue.displayIssue(newArguments);
//        } else if (args[0].equalsIgnoreCase("editIssue")) {
//            Issue.editIssue(newArguments);
//        } else if (args[0].equalsIgnoreCase("removeIssue")) {
//            Issue.removeIssue(newArguments);
        } else {
            runDefault();
        }
    }

    private void runDefault() {
        String select = getString();
        switch (select) {
            case "createProject" -> Project.createProject(null);
//            case "editProject" -> Project.editProject(null);
//            case "deleteProject" -> Project.deleteProject(null);
            case "showAllProjects" -> Project.showAllProjects();
            case "showAllIssues" -> Project.showAllIssues();
            case "selectProject" -> selectedProject = selectProject(null);
            case "selectIssue" -> selectedIssue = selectIssue(null);
            case "createIssue" -> Issue.createIssue(null);
            case "showProject" -> Project.displayProject(null);
            case "showIssue" -> Issue.displayIssue(null);
//            case "editIssue" -> Issue.editIssue(null);
//            case "removeIssue" -> Issue.removeIssue(null);
            case "exit" -> {
            }
        }
    }

    private String getString() {
        String[] options = new String[]{
                "createProject",
//                "editProject",
//                "deleteProject",
                "showAllProjects",
                "showAllIssues",
                "selectProject",
                "selectIssue",
                "createIssue",
                "showProject",
                "showIssue",
//                "editIssue",
//                "removeIssue",
                "exit"
        };
        CLIOptionsSelector<String> optionsSelector = new CLIOptionsSelector<>("ProjectManagementTool", options);
        String select = optionsSelector.selectOption();
        return select;
    }
}
