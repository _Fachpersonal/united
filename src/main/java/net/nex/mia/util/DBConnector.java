package net.nex.mia.util;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

public class DBConnector {

    private static Connection con;

    public DBConnector(String host, String user, String pssw) throws SQLException {
//        con = DriverManager.getConnection(String.format("jdbc:mysql://%s:3306/Mia", host), user, pssw);
        throw new SQLException("\u001B[31m" + "DATABASE Feature not implemented yet!" + "\u001B[0m");
    }

    public static void close() throws SQLException {
        con.close();
    }

    public static ResultSet selectQuery(String statement) throws SQLException {
        return con.createStatement().executeQuery(statement);
    }

    public static int updateQuery(String statement) throws SQLException {
        return con.createStatement().executeUpdate(statement);
    }

    public static void displayResult(ResultSet rs) throws SQLException {
        ResultSetMetaData rsmd = rs.getMetaData();
        System.out.println("-----------------------");
        while (rs.next()) {
            for (int i = 1; i <= rsmd.getColumnCount(); i++) {
                System.out.println(rsmd.getColumnName(i) + " : " + rs.getString(i));
            }
            System.out.println("-----------------------");
        }
    }
}
