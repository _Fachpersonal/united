package net.nex.mia.util;

import net.nex.mia.util.logging.Logger;

import java.io.*;
import java.util.ArrayList;
import java.util.Objects;
import java.util.regex.Pattern;

public class MIAObject {

    private final boolean validObject;
    private final String objectName;
    private final MIAObjectFragment[] fragments;

    public MIAObject(String objectName, MIAObjectFragment... fragments) {
        if (objectName.isBlank() || fragments == null || fragments.length == 0) {
            validObject = false;
            this.objectName = null;
            this.fragments = null;
            return;
        }
        this.objectName = objectName;
        ArrayList<MIAObjectFragment> tempFragments = new ArrayList<>();
        for (MIAObjectFragment frag : fragments) {
            if (frag == null)
                continue;
            tempFragments.add(frag);
        }
        this.fragments = tempFragments.toArray(new MIAObjectFragment[0]);
        validObject = true;
    }

    public static String convertToString(MIAObject miaObject) {
        if (!miaObject.isValidObject()) {
            return null;
        }
        StringBuilder fragmentsAsString = new StringBuilder();
        for (MIAObjectFragment fragment : miaObject.getFragments()) {
            fragmentsAsString.append(String.format("%s,", fragment.toString()));
        }
        fragmentsAsString.deleteCharAt(fragmentsAsString.length() - 1);
        return String.format("%s{%s}", miaObject.getObjectName(), fragmentsAsString);
    }

    public static MIAObject translateToMIAObject(String content) {
        if (!checkPattern(false, false, content)) {
            return null;
        }
        String objectName = content.substring(0, content.indexOf("{"));
        ArrayList<MIAObjectFragment> fragments = new ArrayList<>();

        content = content.substring(objectName.length() + 1); // test{"test":value,"test2":"string"} -> "test":value,"test2":"string"}

        boolean quotation = false;
        boolean nameSelected = true;
        StringBuilder temp = new StringBuilder();
        String fragmentName = "", fragmentValue = "null";
        char[] contentAsChar = content.toCharArray();

        for (char c : contentAsChar) {
            if (c == '\"') {
                if (quotation) {
                    if (nameSelected) {
                        fragmentName = temp.toString();
                    } else {
                        fragmentValue = temp.toString();
                    }
                    temp = new StringBuilder();
                    quotation = false;
                    continue;
                } else {
                    quotation = true;
                    continue;
                }
            }
            if (c == ':' && !quotation) {
                nameSelected = false;
                continue;
            }
            if ((c == ',' || c == '}') && !quotation) {
                fragments.add(new MIAObjectFragment(fragmentName, fragmentValue));
                fragmentName = "";
                fragmentValue = "null";
                nameSelected = true;
                continue;
            }
            temp.append(c);
        }

        MIAObject miaObject = new MIAObject(objectName, fragments.toArray(new MIAObjectFragment[0]));
        return miaObject.isValidObject() ? miaObject : null;
    }

    public static boolean checkPattern(boolean useFragmentPattern, boolean valueIsString, String line) {
        //[A-Za-z]+\{[^}]*\}
        final Pattern pattern = useFragmentPattern ? Pattern.compile("\"[A-Za-z0-9]+\":" + (valueIsString ? "\"[A-Za-z]+\"" : "[A-Za-z]+"), Pattern.CASE_INSENSITIVE) : Pattern.compile("([A-Za-z0-9]+(-[A-Za-z0-9]+)+)\\{[^}]*\\}", Pattern.CASE_INSENSITIVE);
        return pattern.matcher(line).matches();
    }

    public static MIAObject loadFromFile(String path) {
        File file = new File(path);
        if (!file.exists()) {
            return null;
        }
        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            String content = br.readLine();
            MIAObject object = MIAObject.translateToMIAObject(content);
            return object;
        } catch (FileNotFoundException e) {
            Logger.error("File could not be found [%s]", file.getAbsolutePath());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return null;
    }

    public MIAObjectFragment getFragmentByKey(String key) {
        for (MIAObjectFragment fragment : fragments) {
            if (fragment.getName().equalsIgnoreCase(key)) {
                return fragment;
            }
        }
        return null;
    }

    public Object getValueByKey(String key) {
        for (MIAObjectFragment fragment : fragments) {
            if (fragment.getName().equalsIgnoreCase(key)) {
                return fragment.getValue();
            }
        }
        return null;
    }

    public void saveToFile(String filePath, boolean append) throws IOException {
        if (!isValidObject())
            return;
        File file = new File(filePath);
        file.createNewFile();
//        Logger.debug(String.format("File %s [%s]", file.createNewFile() ? "got created" : "got " + (append ? "appended" : "overwritten") + "!", file.getAbsolutePath()));
        BufferedWriter bw = new BufferedWriter(new FileWriter(file, append));
        bw.write(Objects.requireNonNull(convertToString(this)));
        bw.newLine();
        bw.flush();
        bw.close();
    }

    public boolean isValidObject() {
        return validObject;
    }

    public String getObjectName() {
        return objectName;
    }

    public MIAObjectFragment[] getFragments() {
        return fragments;
    }

}
