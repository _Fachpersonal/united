package net.nex.mia.util.logging;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;

public class Logger {

    private static final ArrayList<Log> logs = new ArrayList<>();
    private static boolean logFileDisabled = false;
    private static LogLevel LOG_LEVEL = LogLevel.ERRORS_ONLY;
    private static boolean TIMESTAMP = true;

    public static LogLevel getLogLevel() {
        return LOG_LEVEL;
    }

    public static void setLogLevel(LogLevel logLevel) {
        LOG_LEVEL = logLevel;
        warning(String.format("Set LogLevel to %s", logLevel.toString()));
    }

    public static void toggleTimeStamp() {
        TIMESTAMP = !TIMESTAMP;
        log(String.format("Toggled TimeStamp to %s", TIMESTAMP));
    }

    public static void disableLogFile() {
        logFileDisabled = true;
        log("No log is getting written to file!");
    }

    public static boolean containsTimeStamp() {
        return TIMESTAMP;
    }

    public static Log log(String message) {
        return writeLog(LogState.LOG, message, null);
    }

    public static Log log(String message, String detailedMessage) {
        return writeLog(LogState.LOG, message, detailedMessage);

    }

    public static Log warning(String message) {
        return writeLog(LogState.WARNING, message, null);

    }

    public static Log warning(String message, String detailedMessage) {
        return writeLog(LogState.WARNING, message, detailedMessage);

    }

    public static Log error(String message) {
        return writeLog(LogState.ERROR, message, null);

    }

    public static Log error(String message, String detailedMessage) {
        return writeLog(LogState.ERROR, message, detailedMessage);

    }

    public static Log debug(String message) {
        return writeLog(LogState.DEBUG, message, null);

    }

    public static Log debug(String message, String detailedMessage) {
        return writeLog(LogState.DEBUG, message, detailedMessage);

    }

    private static Log writeLog(LogState state, String message, String detailedMessage) {
        Log logObject = new Log(state, message, detailedMessage, LocalDateTime.now());
        logs.add(logObject);
        logObject.print();
        if (logFileDisabled) {
            return logObject;
        }
        try {
            File logFile = new File("mia.log");
            BufferedWriter bw = new BufferedWriter(new FileWriter(logFile, true));
            bw.write(logObject.toString());
            bw.newLine();
            bw.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return logObject;
    }
}
