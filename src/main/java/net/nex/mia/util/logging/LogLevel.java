package net.nex.mia.util.logging;

public enum LogLevel {
    NONE,
    LOG_ONLY,
    ERRORS_ONLY,
    DEBUG
}
