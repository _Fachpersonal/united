package net.nex.mia.util.logging;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Objects;

public record Log(LogState state, String message, String detailedMessage, LocalDateTime timeStamp) {
    public Log {
        Objects.requireNonNull(state);
        if (message.isBlank() || message.isEmpty()) {
            throw new IllegalArgumentException("Message is empty");
        }
        Objects.requireNonNull(timeStamp);
    }

    public void print() {
        switch (Logger.getLogLevel()) {
            case DEBUG -> System.out.println(this);
            case ERRORS_ONLY -> {
                if (state.equals(LogState.ERROR) || state.equals(LogState.WARNING))
                    System.out.println(this);
            }
            case LOG_ONLY -> {
                if (state.equals(LogState.LOG))
                    System.out.println(this);
            }
            default -> {
            }
        }
    }

    public String toString() {
        StringBuilder string = new StringBuilder();
        final String detailedMessageMark = detailedMessage == null || detailedMessage.isEmpty() || detailedMessage.isBlank() ? "" : "*";
        if (Logger.containsTimeStamp()) {
            string.append(String.format("[%s]%s @ %s >> ", state.toString(), detailedMessageMark, new SimpleDateFormat("HH:mm:ss.SSS").format(new Timestamp(System.currentTimeMillis()))));
        } else {
            string.append(String.format("[%s]%s >> ", state.toString(), detailedMessageMark));
        }
        string.append(message);
        return string.toString();
    }
}
