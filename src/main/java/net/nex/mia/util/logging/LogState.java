package net.nex.mia.util.logging;

public enum LogState {
    LOG,
    WARNING,
    ERROR,
    DEBUG
}
