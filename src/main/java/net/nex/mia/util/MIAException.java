package net.nex.mia.util;

import net.nex.mia.util.logging.Logger;

public class MIAException extends RuntimeException {

    public MIAException(String message) {
        super(message);
        Logger.error(message);
    }
}
