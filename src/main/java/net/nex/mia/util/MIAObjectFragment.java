package net.nex.mia.util;

public class MIAObjectFragment {
    private final String name;
    private final String value;

    public MIAObjectFragment(String name, Object value) {
        this.name = name.isBlank() ? "value" : name;
        this.value = value.toString();
    }

    public String toString() {
        return String.format("\"%s\":\"%s\"", name, value);
    }

    public String getName() {
        return name;
    }

    public Object getValue() {
        return value;
    }
}
