package net.nex.mia.model;

import net.nex.mia.App;
import net.nex.mia.cli.CLI;
import net.nex.mia.cli.CLIHelper;
import net.nex.mia.cli.commands.ProjectManagementCommand;
import net.nex.mia.util.MIAException;
import net.nex.mia.util.MIAObject;
import net.nex.mia.util.MIAObjectFragment;
import net.nex.mia.util.logging.Logger;

import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.UUID;

public record Project(UUID uuid, LocalDate creationDate, ArrayList<Issue> issues, String name, String tag,
                      String description) {

    public static final ArrayList<Project> allProjects = new ArrayList<>();
    private static final String projectDirectory = App.DIRECTORY + "projects/";
    private static final String protectedProject = "InternalIssue";
    private static final String protectedTag = "INTISS";


    public Project {
        if (uuid == null)
            uuid = UUID.randomUUID();
        if (creationDate == null)
            creationDate = LocalDate.now();
        if (issues == null)
            issues = new ArrayList<>();
        if (name.isBlank() || name.isEmpty() || doesNameExists(name))
            throw new MIAException("Given value of name is invalid!");
        if (tag.isBlank() || tag.isEmpty() || doesTagExists(tag) || !checkIfTagIsValid(tag))
            throw new MIAException("Given value of tag is invalid!");

        allProjects.add(this);
        Logger.debug(String.format("Project successfully loaded! [%s] %s", uuid, name));
    }

    public static void loadAllProjects() {
        File f = new File(projectDirectory);
        f.mkdir();
        int counter = 0;
        if (f.isDirectory()) {
            String[] files = f.list();
            if (files == null || files.length == 0) {
                Logger.debug("Found no projects to load");
                createDefaultProject();
                return;
            }
            for (String projectUUID : files) {
                if (projectUUID.equals(".DS_Store"))
                    continue;
                loadProject(projectUUID);
                counter++;
            }
        }
        Logger.debug(String.format("Loaded %d %s!", counter, counter == 1 ? "project" : "projects"));
    }

    public static void loadProject(String uuid) {
        Logger.debug("Trying to load project " + uuid);
        File f = new File(projectDirectory + uuid);
        if (!f.exists() || !f.isDirectory() || !new File(projectDirectory + uuid + "/info.p").exists())
            return;

        MIAObject object = MIAObject.loadFromFile(projectDirectory + uuid + "/info.p");
        if (object == null || !object.isValidObject() || !object.getObjectName().equals(uuid))
            return;

        Object temp;
        UUID _uuid = UUID.fromString(object.getObjectName());
        temp = object.getValueByKey("creationDate");
        LocalDate created = temp == null ? null : LocalDate.parse(String.valueOf(temp));
        String name = String.valueOf(object.getValueByKey("name"));
        String tag = String.valueOf(object.getValueByKey("tag"));
        String description = String.valueOf(object.getValueByKey("description"));
        Project project = new Project(_uuid, created, null, name, tag, description);
        String[] files = f.list();
        if (files == null) {
            return;
        }
        for (String file : files) {
            if (file.equals("info.p") || !file.endsWith(".i")) {
                continue;
            }
            Issue.loadFromFile(projectDirectory + uuid + "/" + file);
        }
        project.saveProject();
    }

    private static void createDefaultProject() {
        Logger.debug("Creating default project!");
        Project project = new Project(null, null, null, protectedProject, protectedTag, "All Internal Issues");
        project.saveProject();
    }

    public static Project getProtectedProject() {
        for (Project project : allProjects) {
            if (project.name.equals(protectedProject) && project.tag.equals(protectedTag)) {
                return project;
            }
        }
        createDefaultProject();
        return getProtectedProject();
    }

    public static Project getProjectByUUID(UUID uuid) {
        for (Project project : allProjects) {
            if (project.uuid.equals(uuid)) {
                return project;
            }
        }
        return getProtectedProject();
    }

    public static void createProject(String[] args) {
        String name, tag, description;
        do {
            System.out.println("Please give a name :");
            name = CLI.instance.getInput().nextLine();
        } while (name.isBlank() || name.isEmpty() || name.length() >= 25);
        do {
            System.out.println("Please give a tag :");
            tag = CLI.instance.getInput().nextLine();
        } while (tag.isBlank() || tag.isEmpty() || tag.length() >= 7);
        tag = tag.toUpperCase();
        do {
            System.out.println("Please give a description :");
            description = CLI.instance.getInput().nextLine();
        } while (description.isBlank() || description.isEmpty());

        Project project = new Project(null, null, null, name, tag, description);
        project.saveProject();
        System.out.println("Project got successfully created :D");
        System.out.println("_+= Project =+_");
        System.out.printf("%s[%s] - %s%n", project.name, project.tag, CLIHelper.getTextPreview(project.description, 75));
    }

    public static void showAllProjects() {
        for (Project oldProject : Project.allProjects) {
            System.out.printf("[%s]%s %s%s - %s%n", oldProject.tag(), CLIHelper.getPadding(oldProject.tag().length() + 2, CLIHelper.TAG_PADDING), oldProject.name(), CLIHelper.getPadding(oldProject.name().length(), CLIHelper.PROJECT_PADDING), CLIHelper.getTextPreview(oldProject.description(), 75));
        }
    }

    public static void showAllIssues() {
        while (ProjectManagementCommand.selectedProject == null) {
            ProjectManagementCommand.selectedProject = ProjectManagementCommand.selectProject(null);
        }
        if (ProjectManagementCommand.selectedProject.issues == null || ProjectManagementCommand.selectedProject.issues.isEmpty()) {
            System.out.println("There are no Issues created yet!");
            return;
        }
        for (Issue issue : ProjectManagementCommand.selectedProject.issues) {
            System.out.printf("%s-%d - %s [%s]%n", ProjectManagementCommand.selectedProject.tag(), issue.index(), issue.title(), issue.state());
        }
    }

    public static void displayProject(String[] args) {
        do {
            ProjectManagementCommand.selectedProject = ProjectManagementCommand.selectProject(null);
        } while (ProjectManagementCommand.selectedProject == null);
        System.out.println("_=+ ProjectView +=_");
        System.out.println("Name : " + ProjectManagementCommand.selectedProject.name);
        System.out.println("Tag : " + ProjectManagementCommand.selectedProject.tag);
        System.out.println("Description : " + ProjectManagementCommand.selectedProject.description);
        Issue[] issues = ProjectManagementCommand.selectedProject.issues.toArray(new Issue[0]);
        int completed = 0;
        for (Issue issue : issues) {
            if (issue.state().equals(IssueState.DONE)) {
                completed++;
            }
        }
        System.out.println("Issues : " + completed + " out of " + issues.length + " completed");
        System.out.println("_=+ ProjectView +=_");
    }

    private void saveProject() {
        File directory = new File(projectDirectory + this.uuid);
        directory.mkdir();
        MIAObject project = new MIAObject(this.uuid.toString(),
                new MIAObjectFragment("name", this.name),
                new MIAObjectFragment("tag", this.tag),
                new MIAObjectFragment("description", this.description),
                new MIAObjectFragment("createdDate", this.creationDate.toString()));
        try {
            project.saveToFile(projectDirectory + this.uuid + "/info.p", false);
        } catch (IOException e) {
            Logger.error(String.format("Failed saving project to file! [%s]", e.getMessage()));
        }

        for (Issue issue : issues) {
            issue.saveIssue(this.uuid, projectDirectory + this.uuid);
        }
    }

    public void addIssue(Issue issue) {
        issues.add(issue);
        Logger.debug(String.format("Added Issue[%s] to Project[%s](%s)", issue.uuid(), uuid, tag));
    }

    private boolean doesNameExists(String name) {
        if (allProjects.isEmpty()) {
            return false;
        }
        for (Project project : allProjects) {
            if (project.name.equalsIgnoreCase(name)) {
                return true;
            }
        }
        return false;
    }

    private boolean doesTagExists(String tag) {
        if (allProjects.isEmpty()) {
            return false;
        }
        for (Project project : allProjects) {
            if (project.tag.equalsIgnoreCase(tag)) {
                return true;
            }
        }
        return false;
    }

    private boolean checkIfTagIsValid(String tag) {
        final int MAX_TAG_LENGTH = 7;
        return !tag.isBlank() && !tag.isEmpty() && tag.length() <= MAX_TAG_LENGTH;
    }

}
