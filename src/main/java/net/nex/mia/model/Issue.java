package net.nex.mia.model;

import net.nex.mia.App;
import net.nex.mia.cli.CLI;
import net.nex.mia.cli.commands.ProjectManagementCommand;
import net.nex.mia.util.MIAException;
import net.nex.mia.util.MIAObject;
import net.nex.mia.util.MIAObjectFragment;
import net.nex.mia.util.logging.Logger;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.UUID;

public record Issue(UUID uuid, int index, String title, String description, IssueState state, LocalDateTime created) {
    public Issue {
        if (uuid == null)
            uuid = UUID.randomUUID();
        if (index <= 0)
            throw new MIAException("Index of issue cannot be beneath or equals 0");
        if (title.isBlank() || title.isEmpty())
            throw new MIAException("Title cannot be empty!");
        if (state == null)
            state = IssueState.OPEN;
        if (created == null)
            created = LocalDateTime.now();
    }

    public static Issue loadFromFile(String path) {
        MIAObject object = MIAObject.loadFromFile(path);
        if (object == null || !object.isValidObject())
            return null;
        Object temp;
        UUID uuid = UUID.fromString(object.getObjectName());
        temp = object.getValueByKey("index");
        int index = temp == null ? -1 : Integer.parseInt((String) temp);
        temp = object.getValueByKey("title");
        String title = temp == null ? null : (String) temp;
        temp = object.getValueByKey("description");
        String description = temp == null ? null : (String) temp;
        temp = object.getValueByKey("state");
        IssueState state = temp == null ? null : IssueState.valueOf(String.valueOf(temp));
        temp = object.getValueByKey("created");
        LocalDateTime created = temp == null ? null : LocalDateTime.parse(String.valueOf(temp));
        temp = object.getValueByKey("projectUUID");
        Project project = temp == null ? Project.getProtectedProject() : Project.getProjectByUUID(UUID.fromString(String.valueOf(temp)));
        Issue issue = new Issue(uuid, index, title, description, state, created);
        project.addIssue(issue);
        return issue;
    }

    public static void createIssue(String[] args) {
        while (ProjectManagementCommand.selectedProject == null) {
            ProjectManagementCommand.selectedProject = ProjectManagementCommand.selectProject(null);
        }

        String title, description;

        do {
            System.out.println("Please give a title :");
            title = CLI.instance.getInput().nextLine();
        } while (title.isEmpty() || title.isBlank());
        do {
            System.out.println("Please give a description :");
            description = CLI.instance.getInput().nextLine();
        } while (description.isEmpty() || description.isBlank());

        Issue issue = new Issue(null, (ProjectManagementCommand.selectedProject.issues().size() + 1), title, description, null, null);
        ProjectManagementCommand.selectedProject.addIssue(issue);
        issue.saveIssue(ProjectManagementCommand.selectedProject.uuid(), App.DIRECTORY + "projects/" + ProjectManagementCommand.selectedProject.uuid());
    }

    public static void displayIssue(String[] args) {
        do {
            do {
                ProjectManagementCommand.selectedProject = ProjectManagementCommand.selectProject(null);
            } while (ProjectManagementCommand.selectedProject == null);
            ProjectManagementCommand.selectedIssue = ProjectManagementCommand.selectIssue(null);
        } while (ProjectManagementCommand.selectedIssue == null);
        System.out.println("_=+ IssueView +=_");
        System.out.println("Name : " + ProjectManagementCommand.selectedIssue.title);
        System.out.println("TAG : " + ProjectManagementCommand.selectedProject.tag() + "-" + ProjectManagementCommand.selectedIssue.index);
        System.out.println("Description : " + ProjectManagementCommand.selectedIssue.description);
        System.out.println("State : " + ProjectManagementCommand.selectedIssue.state.toString());
        System.out.println("Created : " + ProjectManagementCommand.selectedIssue.created.toString());
        System.out.println("_=+ IssueView +=_");
    }

    public void saveIssue(UUID projectUUID, String path) {
        MIAObject object = new MIAObject(this.uuid.toString(),
                new MIAObjectFragment("index", index + ""),
                new MIAObjectFragment("title", title),
                new MIAObjectFragment("description", description),
                new MIAObjectFragment("state", state),
                new MIAObjectFragment("created", created),
                new MIAObjectFragment("projectUUID", projectUUID));
        try {
            object.saveToFile(path + "/" + uuid + ".i", false);
        } catch (IOException e) {
            Logger.error(String.format("Failed saving issue to file! [%s]", e.getMessage()));
        }
    }
}
