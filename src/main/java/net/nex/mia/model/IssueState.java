package net.nex.mia.model;

public enum IssueState {
    OPEN,
    IN_WORK,
    NEEDS_REVIEW,
    DONE
}
